#include <stdio.h>
#include <stdint.h>

uint8_t input[] = {
    66, 82, 66, 117, 75, 91, 86, 87, 31, 51, 222, 187, 112, 236, 9, 98, 34, 69, 0, 198, 150, 29,
    96, 10, 69, 26, 253, 225, 164, 8, 110, 67, 102, 108, 103, 162, 209, 1, 173, 130, 186, 5, 123,
    109, 187, 215, 86, 232, 23, 215, 184, 79, 171, 232, 128, 67, 138, 153, 251, 92, 4, 94, 93,
};

uint8_t func2(uint8_t arg) {
    if (arg <= 2)
	return 1;
    return func2(arg - 1) + func2(arg - 2);
}

uint8_t func3(uint8_t arg) {
    if (arg <= 0)
	return 0;
    if (arg < 0x100)
	return arg;
    return func3(arg - 0x100);
}

uint8_t mod(uint8_t arg) {
    return arg % 0x100;
}

uint8_t fib(uint8_t arg) {
    uint8_t a = 0, b = 1, n = arg;
    while (n > 1) {
	uint8_t c = a;
	a = b;
	b += c;
	n--;
    }
    return (uint8_t)(b & 0xff);
}

void func1(uint8_t arg) {
    char c;
    uint8_t var1, var2;
    if (arg > sizeof(input))
	return;
    if (arg < 0)
	return;
    var1 = input[arg];
//    var2 = func2(arg + 1);
    var2 = fib(arg + 1) % 0x100;
//    c = func3(var2 + arg + var1);
    c = mod(var2 + arg + var1);
    printf("%c", c);
    func1(arg + 1);
}

int main() {
    func1(0);
    return 0;
}
