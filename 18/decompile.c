#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct opcode {
    char *name;		// instruction
    uint8_t code;	// opcode
    int len;		// num of operands
    int lng;		// has uint32_t operand
} opcodes[] = {
    {"Nop",		0, 0},
    {"MovConst",	1, 2, 1},
    {"MovReg",		2, 2},
    {"MathOp",		3, 4},
    {"PushReg",		4, 1},
    {"PopReg",		5, 1},
    {"PushConst",	6, 1, 1},
    {"JmpConst",	7, 1, 1},
    {"PopPc",		8, 0},
    {"Test",		9, 3},
    {"JmpCond",		10, 1, 1},
    {"Call",		11, 1, 1},
    {"Strlen",		0xFC, 0},
    {"CharAt",		0xFD, 0},
    {"Print",		0xFE, 0},
    {"Exit",		0xFF, 0},
    {"Unknown Opcode"},
};

void interpret(FILE *f) {
    static int off = 0;
    uint8_t instr;
    if (fread(&instr, 1, 1, f) > 0) {
	int code = 0;
	for (; code < sizeof(opcodes)/sizeof(struct opcode); code++)
	    if (instr == opcodes[code].code)
		break;
	printf("%04x: %02x %s", off, opcodes[code].code, opcodes[code].name);
	for (int i = 0; i < opcodes[code].len; i++) {
	    uint8_t byte = 0;
	    uint32_t lng = 0;
	    if (i == (opcodes[code].len - 1) && opcodes[code].lng)
		fread(&lng, 4, 1, f);
	    else
		fread(&byte, 1, 1, f);

	    if (instr == 3 && i == 2) {
		char c;
		if (byte == 3)
		    c = '+';
		else if (byte == 4)
		    c = '-';
		else if (byte == 5)
		    c = '*';
		else if (byte == 6)
		    c = '/';
		else
		    c = '?';
		printf(" %c", c);
	    } else if (instr == 9 && i == 1) {
		char *str;
		if (!byte)
		    str = "<";
		else if (byte == 1)
		    str = "<=";
		else
		    str = "?";
		printf(" %s", str);
	    } else if (i == (opcodes[code].len - 1) && opcodes[code].lng) {
		printf(" %08x", lng);
	    } else {
		printf(" %02x", byte);
	    }
	}
	printf("\n");
	off = off + 1 + opcodes[code].len + opcodes[code].lng * 3;
    } else {
	exit(0);
    }
}

int main(int c, char **v) {
    FILE *f;
    if (c < 2)
	printf("%s: Please specify filename\n", v[0]);
    f = fopen(v[1], "rb");
    if (!f) {
	printf("Failed to open file \"%s\"\n", v[1]);
	return 1;
    }
    printf("%s:\n", v[1]);
    while (1)
	interpret(f);
    fclose(f);
    return 0;
}
